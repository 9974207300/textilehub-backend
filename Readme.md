# Textile Hub Backend

### Requirement
- Node.js 8.10
- Serverless ^1.27.3

### Installation
-> Install the dependencies.

```sh
$ npm install
```
or
```sh
$ yarn install (recommended)
```

### Configuration
1. Create ```config.json``` in root directory.
2. Add following ```key: values``` to the file.
```
{
  "dev": {
    "accountId": "<AWS_ACCOUNT_ID>",
    "memorySize": "<AWS_LAMBDA_MEMORY_SIZE>",
    "region": "<AWS_COGNITO_USERPOOL_REGION>",
    "MONGO_URL": "<MLAB_COLLECTION_URL>",
    "userPoolId": "<AWS_COGNITO_USERPOOL_ID>"
  },
  "prod": {
    "accountId": "<AWS_ACCOUNT_ID>",
    "memorySize": "<AWS_LAMBDA_MEMORY_SIZE>",
    "region": "<AWS_COGNITO_USERPOOL_REGION>",
    "MONGO_URL": "<MLAB_COLLECTION_URL>",
    "userPoolId": "<AWS_COGNITO_USERPOOL_ID>"
  }
}
```

### Deployment

-> Create ```deploy.dev ``` file in root directory with below values for deploying to development environment 

```sh
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
export REGION=<AWS_REGION>

serverless deploy --stage dev
```

-> Create ```deploy.prod ``` file in root directory with below values for deploying to production environment 

```sh
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
export REGION=<AWS_REGION>

serverless deploy --stage prod
```

> Run ```./deploy.dev``` or ```./deploy.prod``` in terminal from root directory.

### APIS
- <GET> /user/profile: Fetch user profile, creates profile if not found.

- <POST> /user/profile: Update user profile
```
    body: { firstName, lastName, phone }
```

- <POST> /user/search: Fetch user based on email, phone, firstName, lastName
```
    body: { search }
```

- <GET> /message/: Get user messages grouped by read and unread.

- <POST> /message/: Send message to a user
```
    body: { message, to }
```

- <POST> /message/read: Mark message up to that time read.
```
    body: { date }
```