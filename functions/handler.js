const util = require('util');
const { openDb } = require('./db');

let db;
const response = {
    statusCode: 200,
    headers: {
        'Access-Control-Allow-Origin': '*'
    },
    body: ''
};

module.exports.getProfile = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const username = event.requestContext.authorizer.claims['cognito:username'];
        db = await util.promisify(openDb)();
        const model = db.model('User');
        let User = await model.findOne({ username });
        if (!User) {
            const { email } = event.requestContext.authorizer.claims;
            User = await model.create({ username, email });
        }
        response.statusCode = 200;
        response.body = JSON.stringify(User);
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}

module.exports.updateProfile = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const username = event.requestContext.authorizer.claims['cognito:username'];
        body = JSON.parse(event.body);
        db = await util.promisify(openDb)();
        const model = db.model('User');
        await model.updateOne({ username }, { $set: body });
        response.statusCode = 200;
        response.body = JSON.stringify({ success: true });
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}

module.exports.searchUser = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const username = event.requestContext.authorizer.claims['cognito:username'];
        body = JSON.parse(event.body);
        db = await util.promisify(openDb)();
        const model = db.model('User');
        const Users = await model.find({
            $and: [
                {
                    $or: [
                        { email: new RegExp(body.search, 'i') },
                        { phone: new RegExp(body.search, 'i') },
                        { firstName: new RegExp(body.search, 'i') },
                        { lastName: new RegExp(body.search, 'i') }
                    ]
                },
                {
                    username: { $ne: username }
                }
            ]
        });
        response.statusCode = 200;
        response.body = JSON.stringify(Users);
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}

module.exports.sendMessage = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const username = event.requestContext.authorizer.claims['cognito:username'];
        body = JSON.parse(event.body);
        db = await util.promisify(openDb)();
        const model = db.model('Message');
        const userModel = db.model('User');
        const User = await userModel.findOne({ username }).select('_id');
        body.from = User._id;
        await model.create(body);
        response.statusCode = 200;
        response.body = JSON.stringify({ success: true });
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}

module.exports.getUnreadMessages = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const username = event.requestContext.authorizer.claims['cognito:username'];
        body = JSON.parse(event.body);
        db = await util.promisify(openDb)();
        const model = db.model('Message');
        const [UnreadMessages, ReadMeesage] = await Promise.all([
            model.find({ to: username, read: false }).populate('from'),
            model.find({ to: username, read: true }).populate('from')
        ]);
        response.statusCode = 200;
        response.body = JSON.stringify({ read: ReadMeesage, unRead: UnreadMessages });
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}

module.exports.markRead = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const to = event.requestContext.authorizer.claims['cognito:username'];
        body = JSON.parse(event.body);
        db = await util.promisify(openDb)();
        const model = db.model('Message');
        await model.updateMany({ to, updatedAt: { $lte: body.date } }, { $set: { read: true } });
        response.statusCode = 200;
        response.body = JSON.stringify({ success: true });
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}
