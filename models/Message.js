module.exports = {
    message: { type: String, required: true, },
    from: { type: 'ObjectId', required: true, ref: 'User' },
    to: { type: String, required: true },
    read: { type: Boolean, default: false }
};