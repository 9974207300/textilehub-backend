module.exports = {
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    phone: { type: String },
    firstName: { type: String },
    lastName: { type: String },
};